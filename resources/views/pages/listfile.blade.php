@extends('layouts.main')
@section('title', 'List Files')
@section('content')
<!-- push external head elements to head -->
@push('head')
<link rel="stylesheet" href="{{ asset('plugins/DataTables/datatables.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/jquery-ui/jquery-ui.css') }}">
<style>
    fieldset {
    display: none
}

fieldset.show {
    display: block
}

</style>
@endpush


<div class="container-fluid">
    <div class="page-header">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <i class="ik ik-pie-chart bg-blue"></i>
                    <div class="d-inline">
                        <h5>{{ __('List Files') }}</h5>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <nav class="breadcrumb-container" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <a href="{{route('dashboard')}}"><i class="ik ik-home"></i></a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#">{{ __('Tables') }}</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#">{{ __('List Files') }}</a>
                        </li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>

    <div class="row clearfix">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3>{{ __('List Files') }}</h3>
                </div>
                <div class="card-body">
                    <table id="myAdvancedTable" class="table p-3">
                        <thead>
                            <tr>
                                <th>{{ __('Device name') }}</th>
                                <th>{{ __('bssid') }}</th>
                                <th>{{ __('manuf') }}</th>
                                <th>{{ __('channel') }}</th>
                                <th>{{ __('freqmhz') }}</th>
                                <th>{{ __('maxseenrate') }}</th>
                                <th>{{ __('carrier') }}</th>
                                <th>{{ __('encoding') }}</th>
                                <th>{{ __('packets') }}</th>
                                <th>{{ __('datasize') }}</th>
                                <th>{{ __('bsstimestamp') }}</th>
                                <th>{{ __('first_time') }}</th>
                                <th>{{ __('last_time') }}</th>
                                <th>{{ __('actions') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($networks as $network)
                            <tr>
                                <td>{{ $network->device_name }}</td>
                                <td>{{ $network->bssid }}</td>
                                <td>{{ $network->manuf }}</td>
                                <td>{{ $network->channel }}</td>
                                <td>{{ $network->freqmhz }}</td>
                                <td>{{ $network->maxseenrate }}</td>
                                <td>{{ $network->carrier }}</td>
                                <td>{{ $network->encoding }}</td>
                                <td>{{ $network->packets }}</td>
                                <td>{{ $network->datasize }}</td>
                                <td>{{ $network->bsstimestamp }}</td>
                                <td>{{ $network->first_time }}</td>
                                <td>{{ $network->last_time }}</td>
                                <td><button type="button" data-toggle="modal" data-target="#myModal"
                                        class="btn btn-primary py-2 px-4">Show Details</button> </br>  </br> <button type="button" data-toggle="modal" data-target="#myModal"
                                        class="btn btn-primary py-2 px-4">Show Wireless Client</button></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


</div>
<div id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"
    class="modal fade text-left">
    <div role="document" class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header row d-flex justify-content-between mx-1 mx-sm-3 mb-0 pb-0 border-0">
                <div class="tabs" id="tab01">
                    <h6 class="text-muted">ssid</h6>
                </div>
                <div class="tabs active" id="tab02">
                    <h6 class="font-weight-bold">gps</h6>
                </div>
                <div class="tabs" id="tab03">
                    <h6 class="text-muted">snr</h6>
                </div>
                <div class="tabs" id="tab04">
                    <h6 class="text-muted">cdp device</h6>
                </div>
                <div class="tabs" id="tab05">
                    <h6 class="text-muted">cdp port</h6>
                </div>
            </div>
            <div class="line"></div>
            <div class="modal-body p-0">
                <fieldset id="tab011">
                    <div class="bg-light">
                        <h5 class="text-center mb-4 mt-0 pt-4">Ssid</h5>
                        <pre class="pb-4 json" >
                        {"min-lat":"45.119091","min-lon":"24.382151","min-alt":"0.055000","min-spd":"0.000000","max-lat":"45.119091","max-lon":"24.382151","max-alt":"0.066000","max-spd":"0.000000","peak-lat":"45.119091","peak-lon":"24.382151","peak-alt":"0.066000","avg-lat":"45.119091","avg-lon":"24.382151","avg-alt":"0.066000"}
                        </pre>
                    </div>
                </fieldset>
                <fieldset class="show" id="tab021">
                <div class="bg-light">
                        <h5 class="text-center mb-4 mt-0 pt-4">Gps</h5>
                        <pre class="pb-4 json" >
                        {"min-lat":"45.119091","min-lon":"24.382151","min-alt":"0.055000","min-spd":"0.000000","max-lat":"45.119091","max-lon":"24.382151","max-alt":"0.066000","max-spd":"0.000000","peak-lat":"45.119091","peak-lon":"24.382151","peak-alt":"0.066000","avg-lat":"45.119091","avg-lon":"24.382151","avg-alt":"0.066000"}
                        </pre>
                    </div>
                </fieldset>
                <fieldset id="tab031">
                    <div class="bg-light">
                        <h5 class="text-center mb-4 mt-0 pt-4">Snr</h5>
                    
                    </div>
               
                </fieldset>
                <fieldset id="tab041">
                    <div class="bg-light">
                        <h5 class="text-center mb-4 mt-0 pt-4">cdp device</h5>
                    
                    </div>
               
                </fieldset>
                <fieldset id="tab051">
                    <div class="bg-light">
                        <h5 class="text-center mb-4 mt-0 pt-4">cdp port</h5>

                    </div>
                   
                </fieldset>
                <fieldset id="tab061">
                    <div class="bg-light">
                        <h5 class="text-center mb-4 mt-0 pt-4">cdp port</h5>

                    </div>
                   
                </fieldset>
            </div>
            <div class="line"></div>
           
        </div>
    </div>
</div>
<!-- push external js -->
@push('script')
<script src="{{ asset('plugins/jquery-ui/jquery-ui.min.js') }}"></script>
<script src="{{ asset('plugins/DataTables/datatables.min.js') }}"></script>
<script src="{{ asset('plugins/DataTables/Cell-edit/dataTables.cellEdit.js') }}"></script>
<!--get List Files script-->
<script src="{{ asset('js/editable-datatable.js') }}"></script>
<script>
    $(document).ready(function () {

        $(".tabs").click(function () {

            $(".tabs").removeClass("active");
            $(".tabs h6").removeClass("font-weight-bold");
            $(".tabs h6").addClass("text-muted");
            $(this).children("h6").removeClass("text-muted");
            $(this).children("h6").addClass("font-weight-bold");
            $(this).addClass("active");

            current_fs = $(".active");

            next_fs = $(this).attr('id');
            next_fs = "#" + next_fs + "1";

            $("fieldset").removeClass("show");
            $(next_fs).addClass("show");

            current_fs.animate({}, {
                step: function () {
                    current_fs.css({
                        'display': 'none',
                        'position': 'relative'
                    });
                    next_fs.css({
                        'display': 'block'
                    });
                }
            });
        });
        var element = $(".json");
        var obj = JSON.parse(element.text());
        element.html(JSON.stringify(obj, undefined, 2));

    });

</script>
@endpush
@endsection
