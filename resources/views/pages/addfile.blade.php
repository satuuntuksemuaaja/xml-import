@extends('layouts.main') 
@section('title', 'Add Files')
@section('content')

    <div class="container-fluid">
        <div class="page-header">
            <div class="row align-items-end">
                <div class="col-lg-8">
                    <div class="page-header-title">
                        <i class="ik ik-edit bg-blue"></i>
                        <div class="d-inline">
                            <h5>{{ __('Add Files')}}</h5>
                        </div>
                    </div>
                </div>
   
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header"><h3>{{ __('Add Xml Files')}}</h3></div>
                    <div class="card-body">
                        <form class="forms-sample" method="post" enctype='multipart/form-data' action="{{route('import_files')}}">                                                     
                           @csrf
                        <div class="form-group">
                                <label>{{ __('File upload')}}</label>
                                <input type="file" name="xml[]" class="file-upload-default" multiple>
                                <div class="input-group col-xs-12">
                                    <input type="text" class="form-control file-upload-info" disabled placeholder="Upload Image">
                                    <span class="input-group-append">
                                    <button class="file-upload-browse btn btn-primary" type="button">{{ __('Upload')}}</button>
                                    </span>
                                </div>
                            </div>
                          
                            <button type="submit" class="btn btn-primary mr-2">{{ __('Submit')}}</button>
                            <button class="btn btn-light">{{ __('Cancel')}}</button>
                        </form>
                    </div>
                </div>
            </div>
        
        </div>
    </div>
    
    <!-- push external js -->
    @push('script')
        <script src="{{ asset('js/form-components.js') }}"></script>
    @endpush
@endsection