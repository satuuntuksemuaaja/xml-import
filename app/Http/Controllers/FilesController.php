<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class FilesController extends Controller
{
    public function list_files()
    {
        $networks = DB::table('wireles_network')->join('name_files','name_files.id','wireles_network.name_files')
        ->select('name_files.name_files as device_name','wireles_network.*')
        ->get();

        return view('pages.listfile',compact('networks'));
    }
    public function add_files()
    {
        return view('pages.addfile');
    }
    public function import_files(Request $request)
    {
        $files =$request->file('xml');
        foreach ($files as $key => $value) {
            $file_name = $value->getClientOriginalName();
            $xmlDataString = file_get_contents($value->getRealPath());
            $xmlDataString = str_replace('&', ' ', $xmlDataString);
            $xmlObject = simplexml_load_string($xmlDataString);
            $file_name = explode('-',$file_name)[0];
            $check_file= DB::table('name_files')->where('name_files',$file_name)->first();
            if ($check_file) {
               $id_name = $check_file->id;
            }else{
                $id_name = DB::table('name_files')->insertGetId([
                    'name_files' => $file_name
                ]);
            }

            foreach ($xmlObject as $xml) {
                $json = json_encode($xml);
                $phpArray = json_decode($json, true);

               $insert = [
                   'ssid' => json_encode($xml->SSID),
                   'bssid' => $phpArray['BSSID'],
                   'manuf' => $phpArray['manuf'],
                   'channel' => $phpArray['channel'],
                   'freqmhz' => $phpArray['freqmhz'],
                   'maxseenrate' => $phpArray['maxseenrate'],
                   'carrier' => $phpArray['carrier'],
                   'encoding' => $phpArray['encoding'],
                   'packets' =>json_encode($phpArray['packets']),
                   'datasize' => $phpArray['datasize'],
                   'snr_nfo' =>json_encode($phpArray['snr-info']),
                   'gps_nfo' =>json_encode( $phpArray['gps-info']),
                   'bsstimestamp' => $phpArray['bsstimestamp'],
                   'cdp_device' => json_encode($phpArray['cdp-device']),
                   'cdp_portid' => json_encode($phpArray['cdp-portid']),
                   'name_files' => $id_name,
                   'first_time' => $phpArray['@attributes']['first-time'],
                   'last_time' => $phpArray['@attributes']['last-time'],
               ];
               $wireles_network = DB::table('wireles_network')->insertGetId($insert);
               if(isset($phpArray['wireless-client'])){
                try {
                    $wireles_client = [
                        'payload' => json_encode($phpArray['wireless-client']),
                        'wireless_network' => $wireles_network,
                    ];
                    DB::table('wireles_client')->insert($wireles_client);
                } catch (\Throwable $th) {
                   dd($phpArray['wireless-client'],$th);
                }

               }

            }

        }
        return redirect()->back();
    }
}
